import { ApiProperty } from '@nestjs/swagger';
import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('type_requests')
export class TypeRequest {
  @ApiProperty({
    example: 'e62f69f0-c311-4543-a349-8f7974e3bb2f',
    description: 'Id of type request for front',
    type: String,
  })
  @PrimaryGeneratedColumn('uuid')
  type_requests_id: string;

  @Column('varchar', { nullable: false, unique: true })
  name: string;

  @ApiProperty({
    example: 'Viajes',
    description: 'Name of type request for front',
    type: String,
    name: 'nameForFront',
  })
  @Column('varchar', { nullable: false, unique: true })
  nameForFront: string;

  @ApiProperty({
    example: 'Descripcion de los viajes',
    description: 'description of type request for front',
    type: String,
    name: 'description',
  })
  @Column('varchar', { nullable: false })
  description: string;

  @ApiProperty({
    example: 'Descripcion de los viajes',
    description: 'description of type request for front',
    type: String,
    name: 'logo',
  })
  @Column('varchar', { length: 100, nullable: false })
  logo: string;

  @Column('bool', { default: false })
  active: boolean;

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    select: false,
  })
  created_at: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    select: false,
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  updated_at: Date;
}
