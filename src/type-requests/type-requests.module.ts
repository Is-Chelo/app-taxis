import { Module } from '@nestjs/common';
import { TypeRequestsService } from './type-requests.service';
import { TypeRequestsController } from './type-requests.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TypeRequest } from './entities/type-request.entity';

import { UsersModule } from 'src/users/users.module';
import { PermissionsModule } from 'src/permissions/permissions.module';
import { RolesModule } from 'src/roles/roles.module';

@Module({
  controllers: [TypeRequestsController],
  providers: [TypeRequestsService],
  imports: [
    TypeOrmModule.forFeature([TypeRequest]),
    UsersModule,
    PermissionsModule,
    RolesModule,
  ],
  exports: [TypeRequestsService, TypeOrmModule],
})
export class TypeRequestsModule {}
