import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { handlerDbException } from 'src/common/helpers/handlerDbException';
import { Repository } from 'typeorm';
import { UpdateTypeRequestDto } from './dto/update-type-request.dto';
import { TypeRequest } from './entities/type-request.entity';

@Injectable()
export class TypeRequestsService {
  private readonly logger = new Logger(TypeRequestsService.name);
  constructor(
    @InjectRepository(TypeRequest)
    private readonly typeRequestRepository: Repository<TypeRequest>,
  ) {}
  async findAll() {
    return {
      status: true,
      message: 'success',
      data: await this.typeRequestRepository.find({
        select: {
          type_requests_id: true,
          nameForFront: true,
          logo: true,
          description: true,
          active: true,
        },
      }),
    };
  }

  async findOne(id: string) {
    const typeRequest = await this.typeRequestRepository.findOne({
      where: { type_requests_id: id },
      select: {
        type_requests_id: true,
        nameForFront: true,
        logo: true,
        description: true,
        active: true,
      },
    });

    if (!typeRequest)
      throw new NotFoundException({
        status: false,
        message: `Type Request with id: ${id} not found`,
        error: 'Not Found',
      });

    return {
      message: 'success',
      data: typeRequest,
    };
  }

  async update(id: string, updateTypeRequestDto: UpdateTypeRequestDto) {
    const { name, description, logo } = updateTypeRequestDto;
    await this.findOne(id);

    const typeRequest = await this.typeRequestRepository.preload({
      type_requests_id: id,
      nameForFront: name,
      description,
      logo,
    });
    try {
      await this.typeRequestRepository.save(typeRequest);
      return {
        status: true,
        message: 'success',
        data: typeRequest,
      };
    } catch (error) {
      handlerDbException(error, this.logger);
    }
  }
}
