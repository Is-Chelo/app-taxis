// import { PartialType } from '@nestjs/swagger';
import { PartialType } from '@nestjs/swagger';
import { CreateTypeRequestDto } from './create-type-request.dto';

export class UpdateTypeRequestDto extends PartialType(CreateTypeRequestDto) {}
