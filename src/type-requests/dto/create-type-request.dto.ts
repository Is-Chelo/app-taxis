import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateTypeRequestDto {
  @ApiProperty({
    example: 'Viajes',
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    example: 'Descripcion de los viajes',
    type: String,
  })
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  description: string;

  @ApiProperty({
    example: 'url del logo',
    type: String,
  })
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  logo: string;
}
