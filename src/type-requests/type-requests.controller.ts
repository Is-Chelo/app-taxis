import { Controller, Get, Body, Patch, Param } from '@nestjs/common';
import { TypeRequestsService } from './type-requests.service';
import { UpdateTypeRequestDto } from './dto/update-type-request.dto';
import { Auth } from 'src/users/decorators/auth.decorator';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { getAll } from './swagger/type-request-schemas';
import {
  badRequestException,
  notFoundException,
  unauthorizedException,
} from 'src/common/swagger/schemas';

@ApiTags('Services')
@Controller('type-requests')
export class TypeRequestsController {
  constructor(private readonly typeRequestsService: TypeRequestsService) {}

  @ApiBearerAuth('defaultBearerAuth')
  @ApiOkResponse({
    schema: {
      type: 'array',
      example: [getAll, getAll],
    },
    description: 'Get all type of requests',
  })
  @ApiUnauthorizedResponse({
    schema: unauthorizedException,
    description: 'Unauthorized',
  })
  @Get()
  @Auth()
  findAll() {
    return this.typeRequestsService.findAll();
  }

  @ApiBearerAuth('defaultBearerAuth')
  @ApiOkResponse({
    schema: {
      type: 'object',
      example: getAll,
    },
    description: 'Get One type of requests',
  })
  @ApiNotFoundResponse({
    schema: notFoundException,
    description: 'Type Request not found',
  })
  @ApiUnauthorizedResponse({
    schema: unauthorizedException,
    description: 'Unauthorized',
  })
  @Auth()
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.typeRequestsService.findOne(id);
  }

  @ApiBearerAuth('defaultBearerAuth')
  @ApiOkResponse({
    schema: {
      type: 'object',
      example: getAll,
    },
    description: 'Upodate type of requests',
  })
  @ApiNotFoundResponse({
    schema: notFoundException,
    description: 'Type Request not found',
  })
  @ApiUnauthorizedResponse({
    schema: unauthorizedException,
    description: 'Unauthorized',
  })
  @ApiBadRequestResponse({
    schema: badRequestException,
    description: 'Bad Request',
  })
  @Auth()
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateTypeRequestDto: UpdateTypeRequestDto,
  ) {
    return this.typeRequestsService.update(id, updateTypeRequestDto);
  }
}
