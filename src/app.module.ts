import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommonModule } from './common/common.module';
import { OrganizationsModule } from './organizations/organizations.module';
import { RolesModule } from './roles/roles.module';
import { SeedModule } from './seed/seed.module';
import { UsersModule } from './users/users.module';
import { PermissionsModule } from './permissions/permissions.module';
import { DriversModule } from './drivers/drivers.module';
import { VehicleModelsModule } from './vehicle-models/vehicle-models.module';
import { VehicleBrandsModule } from './vehicle-brands/vehicle-brands.module';
import { TypeRequestsModule } from './type-requests/type-requests.module';
import { DriverVehiclesModule } from './driver-vehicles/driver-vehicles.module';
@Module({
  imports: [
    ConfigModule.forRoot({}),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.DB_HOST,
      port: +process.env.DB_PORT,
      database: process.env.DB_NAME,
      username: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      autoLoadEntities: true,
      synchronize: true,
    }),
    CommonModule,
    UsersModule,
    TypeRequestsModule,
    OrganizationsModule,
    RolesModule,
    SeedModule,
    PermissionsModule,
    DriversModule,
    DriversModule,
    VehicleModelsModule,
    VehicleBrandsModule,
    DriverVehiclesModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
