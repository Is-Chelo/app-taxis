import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ParseUUIDPipe,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiConsumes,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger/dist';

import { OrganizationsService } from './organizations.service';
import { CreateOrganizationDto } from './dto/create-organization.dto';
import { UpdateOrganizationDto } from './dto/update-organization.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { fileFilter } from 'src/common/helpers/fileFilter.helper';
import { diskStorage } from 'multer';
import { fileNamer } from 'src/common/helpers/fileNamer.helper';
import { organizationSchema } from './swagger/organization-schema';
import {
  badRequestException,
  notFoundException,
  unauthorizedException,
} from 'src/common/swagger/schemas';
import { Auth } from 'src/users/decorators/auth.decorator';

@ApiTags('Organizations')
@ApiBadRequestResponse({
  schema: badRequestException,
  description: 'Bad request',
})
@ApiUnauthorizedResponse({
  schema: unauthorizedException,
  description: 'Unauthorized',
})
@ApiBearerAuth('defaultBearerAuth')
@Controller('organizations')
export class OrganizationsController {
  constructor(private readonly organizationsService: OrganizationsService) {}

  @ApiCreatedResponse({
    schema: {
      type: 'object',
      example: {
        status: true,
        message: 'Organization creted successfully',
        data: organizationSchema,
      },
    },
    description: 'Organization create.',
  })
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(
    FileInterceptor('logo', {
      fileFilter: fileFilter,
      storage: diskStorage({
        destination: './static/organizations/logo',
        filename: fileNamer,
      }),
    }),
  )
  @Auth()
  @Post()
  create(
    @UploadedFile() logo: Express.Multer.File,
    @Body() createOrganizationDto: CreateOrganizationDto,
  ) {
    return this.organizationsService.create(createOrganizationDto, logo);
  }

  @ApiOkResponse({
    schema: {
      type: 'object',
      example: {
        status: true,
        message: 'success',
        data: [organizationSchema, organizationSchema],
      },
    },
    description: 'Get All Organizations.',
  })
  @Auth()
  @Get()
  findAll() {
    return this.organizationsService.findAll();
  }

  @ApiOkResponse({
    schema: {
      type: 'object',
      example: {
        status: true,
        message: 'success',
        data: organizationSchema,
      },
    },
    description: 'Get One Organization.',
  })
  @ApiNotFoundResponse({
    schema: notFoundException,
    description: 'Not found',
  })
  @Auth()
  @Get(':id')
  findOne(@Param('id', ParseUUIDPipe) id: string) {
    return this.organizationsService.findOne(id);
  }

  @ApiOkResponse({
    schema: {
      type: 'object',
      example: {
        status: true,
        message: 'Organization creted successfully',
        data: organizationSchema,
      },
    },
    description: 'Organization create.',
  })
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(
    FileInterceptor('logo', {
      fileFilter: fileFilter,
      storage: diskStorage({
        destination: './static/organizations/logo',
        filename: fileNamer,
      }),
    }),
  )
  @Patch(':id')
  update(
    @UploadedFile() logo: Express.Multer.File,
    @Param('id', ParseUUIDPipe) id: string,
    @Body() updateOrganizationDto: UpdateOrganizationDto,
  ) {
    return this.organizationsService.update(id, updateOrganizationDto, logo);
  }

  @ApiOkResponse({
    schema: {
      type: 'object',
      example: {
        status: true,
        message: 'success',
      },
    },
    description: 'Delete Organization.',
  })
  @ApiNotFoundResponse({
    schema: notFoundException,
    description: 'Not found',
  })
  @Auth()
  @Delete(':id')
  remove(@Param('id', ParseUUIDPipe) id: string) {
    return this.organizationsService.remove(id);
  }
}
