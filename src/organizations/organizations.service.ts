import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { deleteImage } from 'src/common/helpers/deleteImage';
import { handlerDbException } from 'src/common/helpers/handlerDbException';
import { Repository } from 'typeorm';
import { CreateOrganizationDto } from './dto/create-organization.dto';
import { UpdateOrganizationDto } from './dto/update-organization.dto';
import { Organization } from './entities/organization.entity';

@Injectable()
export class OrganizationsService {
  private readonly logger = new Logger(OrganizationsService.name);

  constructor(
    @InjectRepository(Organization)
    private readonly organizationRepository: Repository<Organization>,
  ) {}

  async create(
    createOrganizationDto: CreateOrganizationDto,
    logo: Express.Multer.File,
  ) {
    try {
      const organization = this.organizationRepository.create({
        ...createOrganizationDto,
        logo: logo
          ? `${process.env.HOST_URL}/organizations/image/${logo.filename}`
          : null,
      });
      await this.organizationRepository.save(organization);
      return {
        status: true,
        message: 'Organization creted successfully',
        data: organization,
      };
    } catch (error) {
      handlerDbException(error, this.logger);
    }
  }

  async findAll() {
    const data = await this.organizationRepository.find();
    return {
      status: true,
      message: 'success',
      data,
    };
  }

  async findOne(id: string) {
    const organization = await this.organizationRepository.findOneBy({
      organization_id: id,
    });
    if (!organization)
      throw new NotFoundException({
        status: false,
        message: `Organization with id: ${id} not found`,
        error: 'Not Found',
      });
    return {
      status: true,
      message: 'success',
      data: organization,
    };
  }

  async update(
    id: string,
    updateOrganizationDto: UpdateOrganizationDto,
    logo: Express.Multer.File,
  ) {
    if (logo) {
      // TODO: Delete image
      await deleteImage(
        { organization_id: id },
        'logo',
        'static/organizations/logo',
        this.organizationRepository,
      );
    }

    const organization = await this.organizationRepository.preload({
      organization_id: id,
      ...updateOrganizationDto,
      logo:
        logo && `${process.env.HOST_URL}/organizations/image/${logo.filename}`,
    });

    if (!organization)
      throw new NotFoundException({
        status: false,
        message: `Organization with id: ${id} not found`,
        error: 'Not Found',
      });
    try {
      this.organizationRepository.save(organization);
      return {
        status: true,
        message: 'Organization updated',
        data: organization,
      };
    } catch (error) {
      handlerDbException(error, this.logger);
    }
  }

  async remove(id: string) {
    const organization = await this.findOne(id);
    this.organizationRepository.remove(organization.data);
    return {
      status: true,
      message: 'Organization deleted',
    };
  }
}
