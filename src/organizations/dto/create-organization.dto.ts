import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsNotEmpty,
  IsNumberString,
  IsOptional,
  IsString,
  MinLength,
} from 'class-validator';

export class CreateOrganizationDto {
  @ApiProperty({
    example: 'Central Taxis',
    description: 'Organization name',
    minLength: 1,
    nullable: false,
  })
  @IsString()
  @IsNotEmpty()
  @MinLength(1)
  name: string;

  @ApiProperty({
    example: 'Calle La paz N#10',
    description: 'Direccion de la empresa',
    minLength: 1,
    nullable: false,
  })
  @IsString()
  @IsNotEmpty()
  @MinLength(1)
  address: string;

  @ApiProperty({
    example: '77554433',
    description: 'Numero de celular de la empresa',
    nullable: false,
  })
  @IsString()
  @IsNotEmpty()
  @IsNumberString()
  cell_phone_number: string;

  @ApiProperty({
    example: '64-44334',
    description: 'Numero de Telefono de la empresa',
    nullable: false,
  })
  @IsString()
  @IsNotEmpty()
  phone_number: string;

  @ApiProperty({
    example: 'central@gmail.com',
    description: 'Correo electronico de la empresa',
    nullable: false,
  })
  @IsString()
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @ApiProperty({
    type: 'string',
    format: 'binary',
    required: false,
    description: 'Logo de la empresa',
  })
  @IsOptional()
  logo?: string;

  @ApiProperty({
    example: 'https://www.facebook.com/central',
    description: 'Link de la pagina de facebook de la empresa',
    required: false,
  })
  @IsString()
  @IsOptional()
  link_face?: string;

  @ApiProperty({
    example: 'https://www.instagram.com/central',
    description: 'Link de la pagina de instagram de la empresa',
    required: false,
  })
  @IsString()
  @IsOptional()
  link_instagram?: string;

  @ApiProperty({
    description: 'Activar la empresa',
    required: false,
  })
  // @IsBoolean()
  @IsOptional()
  active?: boolean;
}
