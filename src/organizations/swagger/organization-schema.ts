export const organizationSchema = {
  organization_id: '70460946-7da8-4a5c-a683-f32a6a4f0cc8',
  name: 'Organizacion Nueva',
  address: 'nueva direccion',
  email: 'chelo@gmail.com',
  cell_phone_number: '591332243',
  phone_number: '64-34242',
  logo: 'http://localhost:3000/api/v1/organizations/image/5f5ac587-aad7-4fb0-a8f8-02c3193dc2bb.jpeg',
  link_face: null,
  link_instagram: null,
  active: true,
};
