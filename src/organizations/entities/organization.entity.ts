import { User } from 'src/users/entities/user.entity';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'organizations' })
export class Organization {
  @PrimaryGeneratedColumn('uuid')
  organization_id: string;

  @Column('varchar', { length: 100, unique: true, nullable: false })
  name: string;

  @Column('varchar', { length: 254, nullable: false })
  address: string;

  @Column('varchar', { length: 100, unique: true, nullable: false })
  email: string;

  @Column('varchar', { nullable: false })
  cell_phone_number: string;

  @Column('varchar', { nullable: false })
  phone_number: string;

  @Column('varchar', { length: 100, nullable: true })
  logo?: string;

  @Column('varchar', { length: 100, nullable: true })
  link_face?: string;

  @Column('varchar', { length: 100, nullable: true })
  link_instagram?: string;

  @Column('bool', { default: true, nullable: false })
  active: boolean;

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    select: false,
  })
  created_at: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
    select: false,
  })
  updated_at: Date;

  @OneToMany(() => User, (organization) => organization.organization)
  users: User[];

  @BeforeInsert()
  checkFieldsBeforeInsert() {
    this.email = this.email.toLowerCase().trim();
  }

  @BeforeUpdate()
  checkFieldsBeforeUpdate() {
    this.checkFieldsBeforeInsert();
  }
}
