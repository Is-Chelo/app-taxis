import { IsInt, IsNotEmpty, IsString, MinLength } from 'class-validator';

export class CreateVehicleModelDto {
  @IsNotEmpty()
  @IsString()
  @MinLength(1)
  name: string;

  @IsNotEmpty()
  @IsInt()
  vehicle_brand: string;
}
