import { Module } from '@nestjs/common';
import { VehicleModelsService } from './vehicle-models.service';
import { VehicleModelsController } from './vehicle-models.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { VehicleModel } from './entities/vehicle-model.entity';
import { VehicleBrandsModule } from 'src/vehicle-brands/vehicle-brands.module';

@Module({
  controllers: [VehicleModelsController],
  providers: [VehicleModelsService],
  imports: [TypeOrmModule.forFeature([VehicleModel]), VehicleBrandsModule],
  exports: [TypeOrmModule, VehicleModelsService],
})
export class VehicleModelsModule {}
