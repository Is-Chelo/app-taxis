import { VehicleBrand } from 'src/vehicle-brands/entities/vehicle-brand.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('vehicle_models')
export class VehicleModel {
  @PrimaryGeneratedColumn()
  model_id: string;

  @Column('varchar', { length: 100, nullable: false })
  name: string;

  @Column('bool', { default: true })
  active: boolean;

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    select: false,
  })
  created_at: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    select: false,
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  updated_at: Date;

  @ManyToOne(
    () => VehicleBrand,
    (vehicle_brand) => vehicle_brand.vehicle_models,
    {
      eager: true,
    },
  )
  vehicle_brand: VehicleBrand;
}
