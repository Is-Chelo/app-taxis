import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PaginationDto } from 'src/common/dtos/pagination.dto';
import { VehicleBrandsService } from 'src/vehicle-brands/vehicle-brands.service';
import { Repository } from 'typeorm';
import { CreateVehicleModelDto } from './dto/create-vehicle-model.dto';
import { UpdateVehicleModelDto } from './dto/update-vehicle-model.dto';
import { VehicleModel } from './entities/vehicle-model.entity';

@Injectable()
export class VehicleModelsService {
  private readonly logger = new Logger(VehicleModelsService.name);
  constructor(
    @InjectRepository(VehicleModel)
    private readonly vehicleModelRepository: Repository<VehicleModel>,

    private readonly vehicleBrandService: VehicleBrandsService,
  ) {}

  async create(createVehicleModelDto: CreateVehicleModelDto) {
    const { vehicle_brand, ...rest } = createVehicleModelDto;
    const brandVehicle = await this.vehicleBrandService.findOne(vehicle_brand);

    const modelVehicle = this.vehicleModelRepository.create({
      ...rest,
      vehicle_brand: brandVehicle.data,
    });
    try {
      await this.vehicleModelRepository.save(modelVehicle);
      return {
        message: 'VehicleModel saved successfully',
        data: modelVehicle,
      };
    } catch (error) {
      this.handlerDbException(error);
    }
  }

  async findAll(paginationDto: PaginationDto) {
    return {
      message: 'success',
      data: await this.vehicleModelRepository.find({
        take: paginationDto.limit,
        skip: paginationDto.offset,
      }),
    };
  }

  async findOne(id: string) {
    const user = await this.vehicleModelRepository.findOneBy({ model_id: id });
    if (!user)
      throw new NotFoundException(`Vehicle - Model with id: ${id} not found`);
    return {
      message: 'success',
      data: user,
    };
  }

  async update(id: string, updateVehicleModelDto: UpdateVehicleModelDto) {
    const { vehicle_brand, ...rest } = updateVehicleModelDto;
    const brandVehicle = await this.vehicleBrandService.findOne(vehicle_brand);
    const model = await this.vehicleModelRepository.preload({
      model_id: id,
      ...rest,
      vehicle_brand: brandVehicle.data,
    });
    if (!model) {
      throw new NotFoundException('Vehicle - Model not found');
    }
    try {
      await this.vehicleModelRepository.save(model);
      return {
        message: 'Vehicle - Model updated',
        data: model,
      };
    } catch (error) {
      this.handlerDbException(error);
    }
  }

  async remove(id: string) {
    const model = await this.findOne(id);
    this.vehicleModelRepository.remove(model.data);
    return {
      message: 'Vehicle - Model removed',
    };
  }

  private handlerDbException(error: any) {
    this.logger.error(error);

    if (error.code === '23505') {
      throw new BadRequestException(error.detail);
    }
    throw new InternalServerErrorException();
  }
}
