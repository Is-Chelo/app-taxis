import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { PaginationDto } from 'src/common/dtos/pagination.dto';
import { DriverVehiclesService } from './driver-vehicles.service';
import { CreateDriverVehicleDto } from './dto/create-driver-vehicle.dto';
import { UpdateDriverVehicleDto } from './dto/update-driver-vehicle.dto';

@Controller('driver-vehicles')
export class DriverVehiclesController {
  constructor(private readonly driverVehiclesService: DriverVehiclesService) {}

  @Post()
  create(@Body() createDriverVehicleDto: CreateDriverVehicleDto) {
    return this.driverVehiclesService.create(createDriverVehicleDto);
  }

  @Get()
  findAll(@Query() pagination: PaginationDto) {
    return this.driverVehiclesService.findAll(pagination);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.driverVehiclesService.findOne(id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateDriverVehicleDto: UpdateDriverVehicleDto,
  ) {
    return this.driverVehiclesService.update(id, updateDriverVehicleDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.driverVehiclesService.remove(id);
  }
}
