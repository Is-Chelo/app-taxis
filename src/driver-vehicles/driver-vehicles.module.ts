import { Module } from '@nestjs/common';
import { DriverVehiclesService } from './driver-vehicles.service';
import { DriverVehiclesController } from './driver-vehicles.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DriverVehicle } from './entities/driver-vehicle.entity';

@Module({
  controllers: [DriverVehiclesController],
  providers: [DriverVehiclesService],
  imports: [TypeOrmModule.forFeature([DriverVehicle])],
})
export class DriverVehiclesModule {}
