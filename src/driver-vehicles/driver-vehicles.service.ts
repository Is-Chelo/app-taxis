import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PaginationDto } from 'src/common/dtos/pagination.dto';
import { Repository } from 'typeorm';
import { CreateDriverVehicleDto } from './dto/create-driver-vehicle.dto';
import { UpdateDriverVehicleDto } from './dto/update-driver-vehicle.dto';
import { DriverVehicle } from './entities/driver-vehicle.entity';

@Injectable()
export class DriverVehiclesService {
  constructor(
    @InjectRepository(DriverVehicle)
    private readonly driverVehicleRepository: Repository<DriverVehicle>,
  ) {}
  create(createDriverVehicleDto: CreateDriverVehicleDto) {
    return 'This action adds a new driverVehicle';
  }

  async findAll(pagination: PaginationDto) {
    const { limit, offset } = pagination;
    const vehicles = this.driverVehicleRepository.find({
      skip: offset,
      take: limit,
    });
    return {
      message: 'success',
      data: vehicles,
    };
  }

  findOne(id: string) {
    return `This action returns a #${id} driverVehicle`;
  }

  update(id: string, updateDriverVehicleDto: UpdateDriverVehicleDto) {
    return `This action updates a #${id} driverVehicle`;
  }

  remove(id: string) {
    return `This action removes a #${id} driverVehicle`;
  }
}
