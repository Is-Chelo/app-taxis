import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('driver_vehicles')
export class DriverVehicle {
  @PrimaryGeneratedColumn('uuid')
  driver_vehicle_id: string;

  @Column('int', { nullable: false })
  license_plate: number;

  @Column('varchar', { nullable: false })
  vehicle_photo: string;

  @Column('varchar', { length: 100, nullable: false })
  color: string;

  @Column('varchar', { length: 100, nullable: false })
  photo_soat: string;

  @Column('bool', { default: true })
  active: boolean;

  @Column('int', { nullable: false })
  model_year: number;

  //   driver_id: string;
}
