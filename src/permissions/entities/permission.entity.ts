import { RolePermission } from 'src/roles/entities/role_permissions.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('permissions')
export class Permission {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar', { length: 100, unique: true, nullable: false })
  name: string;

  @Column('varchar')
  description: string;

  @Column('varchar', { length: 100, unique: true, nullable: false })
  url: string;

  @Column('bool', { default: true })
  active: boolean;

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    select: false,
  })
  created_at: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
    select: false,
  })
  updated_at: Date;

  @OneToMany(
    () => RolePermission,
    (role_permission) => role_permission.permission,
  )
  role_permissions: RolePermission[];
}
