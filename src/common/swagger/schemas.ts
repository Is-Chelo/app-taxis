export const badRequestException = {
  type: 'object',
  example: {
    status: false,
    message: 'string',
    error: 'Bad Request',
  },
};

export const notFoundException = {
  type: 'object',
  example: {
    status: false,
    message: 'string',
    error: 'Not Found',
  },
};

export const unauthorizedException = {
  type: 'object',
  example: {
    status: false,
    message: 'string',
    error: 'Unauthorized',
  },
};
