import {
  BadRequestException,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';

export const handlerDbException = (error: any, logger: Logger) => {
  logger.error(error);
  if (error.code === '23505')
    throw new BadRequestException({
      status: false,
      message: error.detail,
      error: 'Bad Request',
    });

  if (error.code === '23502')
    throw new BadRequestException({
      status: false,
      message: error.column + ' is required',
      error: 'Bad Request',
    });

  throw new InternalServerErrorException({
    status: false,
    message: 'Internal Server Error',
    error: 'Internal Server Error',
  });
};
