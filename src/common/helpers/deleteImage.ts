import { unlink } from 'fs';
import { join } from 'path';

import { Repository } from 'typeorm';

/**
 * Delete image from the server
 * @param where - where clause
 * @param nameField - name of the field that contains the image name
 * @param store - store where the image is located
 * @param repository - repository of the entity
 */
export const deleteImage = async (
  where: object,
  nameField: string,
  store: string,
  repository: Repository<any>,
) => {
  const response = await repository.findOne({
    where,
  });
  const nameImage = response[nameField].split('/').pop();
  if (nameImage) {
    const path = join(__dirname, `../../../${store}/${nameImage}`);
    unlink(path, (err) => {
      if (err) console.log('File not found');
      console.log('File deleted!');
    });
  }
};
