/**
 * Image file filter
 * @param req REQUEST
 * @param file file to validate
 * @param callback callback
 * @returns callback
 */

export const fileFilter = (
  req: Express.Request,
  file: Express.Multer.File,
  callback: any,
) => {
  if (!file) return callback(new Error('File is empty'), false);
  const fileExtenion = file.mimetype.split('/')[1];

  const validExtension = ['jpg', 'jpeg', 'png', 'gif'];

  if (!validExtension.includes(fileExtenion))
    return callback(new Error('Invalid file extension'), false);

  callback(null, true);
};
