import { Permission } from 'src/permissions/entities/permission.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Role } from './role.entity';

@Entity('role_permissions')
export class RolePermission {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('bool', { nullable: false })
  select: boolean;

  @Column('bool', { nullable: false })
  update: boolean;

  @Column('bool', { nullable: false })
  create: boolean;

  @Column('bool', { nullable: false })
  delete: boolean;

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    select: false,
  })
  created_at: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
    select: false,
  })
  updated_at: Date;

  @ManyToOne(() => Role, (role) => role.role_permissions)
  role: Role;

  @ManyToOne(() => Permission, (permission) => permission.role_permissions)
  permission: Permission;
}
