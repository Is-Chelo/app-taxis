import {
  BadRequestException,
  ConflictException,
  Injectable,
  InternalServerErrorException,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';
import { UsersService } from 'src/users/users.service';
import { DataSource, Repository } from 'typeorm';
import { CreateDriverDto } from './dto/create-driver.dto';
import { UpdateDriverDto } from './dto/update-driver.dto';
import { Driver } from './entities/driver.entity';
import * as bcrypt from 'bcrypt';
import { Roles } from 'src/users/enums/roles.enum';
import { Role } from 'src/roles/entities/role.entity';
import { join } from 'path';
import { existsSync } from 'fs';
import { handlerDbException } from 'src/common/helpers/handlerDbException';
@Injectable()
export class DriversService {
  private readonly logger = new Logger(DriversService.name);
  constructor(
    @InjectRepository(Driver)
    private readonly driversRepository: Repository<Driver>,

    @InjectRepository(Role)
    private readonly roleRepository: Repository<Role>,

    private readonly usersService: UsersService,

    private readonly dataSource: DataSource,
  ) {}

  async create(
    createDriverDto: CreateDriverDto,
    files: object,
    userAuth: User,
  ) {
    const {
      name,
      last_name,
      ci_number,
      ci_extention,
      ci_expedition,
      cel_phone,
      code_cel_phone,
      date_birth,
      email,
      password,
      ...driverRes
    } = createDriverDto;
    const queryRunner = this.dataSource.createQueryRunner();

    // TODO: validate if user exists
    await this.usersService.validateUserExists(email, ci_number, cel_phone);

    const role = await this.roleRepository.findOne({
      where: { name: Roles.DRIVER },
    });

    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      const user = await queryRunner.manager.save(User, {
        name,
        last_name,
        ci_number,
        ci_extention,
        ci_expedition,
        cel_phone,
        code_cel_phone,
        date_birth,
        email,
        password: await bcrypt.hash(password, 10),
        is_verified: true,
        role,
        organization: userAuth.organization,
      });

      // TODO: Create driver
      const driver = this.driversRepository.create({
        ...driverRes,
        user,
        license_photo_back: `${process.env.HOST_URL}/drivers/image/${files['license_photo_back']}`,
        license_photo_front: `${process.env.HOST_URL}/drivers/image/${files['license_photo_front']}`,
        face_photo: `${process.env.HOST_URL}/drivers/image/${files['face_photo']}`,
      });

      await queryRunner.manager.save(Driver, driver);
      await queryRunner.commitTransaction();

      return {
        message: 'Driver saved successfully',
        data: {
          user_id: user.user_id,
          name: user.name,
          last_name: user.last_name,
          cel_phone: user.cel_phone,
          code_cel_phone: user.code_cel_phone,
          movil_number: driver.movil_number,
          face_photo: driver.face_photo,
          license_photo_back: driver.license_photo_back,
          license_photo_front: driver.license_photo_front,
          license_number: driver.license_number,
          license_expiration_date: driver.license_expiration_date,
          role,
        },
      };
    } catch (error) {
      await queryRunner.rollbackTransaction();
      handlerDbException(error, this.logger);
    } finally {
      await queryRunner.release();
    }
  }

  async findAll() {
    const drivers = await this.driversRepository
      .createQueryBuilder('drivers')
      .leftJoinAndSelect('drivers.user', 'user')
      .leftJoinAndSelect('user.role', 'role')
      .select([
        'drivers.id driver_id',
        'user.user_id user_id',
        'drivers.active driver_active',
        'user.name name',
        'user.last_name last_name',
        'user.cel_phone cel_phone',
        'user.code_cel_phone code_cel_phone',
        'user.email email',
        'user.date_birth date_birth',
        'user.ci_number ci_number',
        'user.ci_extention ci_extention',
        'user.ci_expedition ci_expedition',
        'drivers.movil_number movil_number',
        'drivers.face_photo face_photo',
        'drivers.license_photo_back license_photo_back',
        'drivers.license_photo_front license_photo_front',
        'drivers.license_number license_number',
        'role.name role',
      ])
      .execute();
    return {
      message: 'success',
      data: drivers,
    };
  }

  async findOne(id: string) {
    const driver = await this.driversRepository
      .createQueryBuilder('drivers')
      .leftJoinAndSelect('drivers.user', 'user')
      .leftJoinAndSelect('user.role', 'role')
      .select([
        'drivers.id driver_id',
        'user.user_id user_id',
        'drivers.active driver_active',
        'user.name name',
        'user.last_name last_name',
        'user.cel_phone cel_phone',
        'user.code_cel_phone code_cel_phone',
        'user.email email',
        'user.date_birth date_birth',
        'user.ci_number ci_number',
        'user.ci_extention ci_extention',
        'user.ci_expedition ci_expedition',
        'drivers.movil_number movil_number',
        'drivers.face_photo face_photo',
        'drivers.license_photo_back license_photo_back',
        'drivers.license_photo_front license_photo_front',
        'drivers.license_number license_number',
        'role.name role',
      ])
      .where('drivers.id = :id', { id })
      .execute();
    if (!driver) throw new NotFoundException(`Driver with id: ${id} not found`);
    return {
      message: 'success',
      data: driver,
    };
  }

  update(id: number, updateDriverDto: UpdateDriverDto) {
    return `This action updates a #${id} driver`;
  }

  async remove(id: string) {
    const driver = await this.driversRepository.findOne({
      where: { id },
      relations: { user: true },
    });
    if (!driver) throw new NotFoundException(`Driver with id: ${id} not found`);
    await this.driversRepository.remove(driver);
    await this.usersService.remove(driver.user.user_id);
    return {
      message: 'Driver deleted successfully',
    };
  }

  async handlerDbError(error: any) {
    console.log(error);
    this.logger.error(error);
    const { code, detail } = error;
    switch (code) {
      case '23505':
        throw new ConflictException(detail);
      case '23502':
        throw new BadRequestException(detail);
      default:
        throw new InternalServerErrorException(detail);
    }
  }

  // TODO: PHOTO services
  getImageDriver(photo: string) {
    const path = join(__dirname, '../../static/drivers', photo);
    if (!existsSync(path)) throw new BadRequestException('Photo not found');
    return path;
  }
}
