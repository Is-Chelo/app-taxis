import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseInterceptors,
  BadRequestException,
  UploadedFiles,
  ParseUUIDPipe,
  Res,
} from '@nestjs/common';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { diskStorage } from 'multer';
import { fileFilter } from 'src/common/helpers/fileFilter.helper';
import { fileNamer } from 'src/common/helpers/fileNamer.helper';
import { Auth } from 'src/users/decorators/auth.decorator';
import { GetUser } from 'src/users/decorators/get-user.decorator';
import { User } from 'src/users/entities/user.entity';
import { DriversService } from './drivers.service';
import { CreateDriverDto } from './dto/create-driver.dto';
import { UpdateDriverDto } from './dto/update-driver.dto';

@ApiTags('Drivers')
@Controller('drivers')
export class DriversController {
  constructor(private readonly driversService: DriversService) {}

  @Auth()
  @Post()
  @UseInterceptors(
    FileFieldsInterceptor(
      [
        { name: 'face_photo', maxCount: 1 },
        { name: 'license_photo_back', maxCount: 1 },
        { name: 'license_photo_front', maxCount: 1 },
      ],
      {
        fileFilter: fileFilter,
        storage: diskStorage({
          destination: './static/drivers',
          filename: fileNamer,
        }),
      },
    ),
  )
  create(
    @Body() createDriverDto: CreateDriverDto,
    @GetUser() userAuth: User,
    @UploadedFiles()
    files: {
      face_photo: Express.Multer.File[];
      license_photo_back: Express.Multer.File[];
      license_photo_front: Express.Multer.File[];
    },
  ) {
    if (!files) throw new BadRequestException('Make sure to upload a files');
    else {
      if (!files.face_photo)
        throw new BadRequestException('face_photo should not be empty');
      if (!files.license_photo_back)
        throw new BadRequestException('license_photo_back should not be empty');
      if (!files.license_photo_front)
        throw new BadRequestException(
          'license_photo_front should not be empty',
        );
    }

    return this.driversService.create(
      createDriverDto,
      {
        face_photo: files.face_photo[0].filename,
        license_photo_back: files.license_photo_back[0].filename,
        license_photo_front: files.license_photo_front[0].filename,
      },
      userAuth,
    );
  }

  @Auth()
  @Get()
  findAll() {
    return this.driversService.findAll();
  }

  @Get('image/:photo')
  getPhotoUser(@Res() res: Response, @Param('photo') photo: string) {
    const path = this.driversService.getImageDriver(photo);
    res.sendFile(path);
  }

  @Auth()
  @Get(':id')
  findOne(@Param('id', ParseUUIDPipe) id: string) {
    return this.driversService.findOne(id);
  }

  @Auth()
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateDriverDto: UpdateDriverDto) {
    return this.driversService.update(+id, updateDriverDto);
  }

  @Auth()
  @Delete(':id')
  remove(@Param('id', ParseUUIDPipe) id: string) {
    return this.driversService.remove(id);
  }
}
