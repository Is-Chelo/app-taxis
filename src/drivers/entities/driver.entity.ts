import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('drivers')
export class Driver {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('int', { nullable: false, unique: true })
  movil_number: number;

  @Column('varchar', { length: 100, nullable: false })
  face_photo: string;

  @Column('varchar', { length: 100, nullable: false })
  license_photo_back: string;

  @Column('varchar', { length: 100, nullable: false })
  license_photo_front: string;

  @Column('varchar', { length: 100, nullable: false })
  license_number: string;

  @Column('date', { nullable: false })
  license_expiration_date: Date;

  @Column('bool', { nullable: true })
  delivery_money?: boolean;

  @Column('int', { nullable: true })
  amount_delivery_money?: number;

  @Column('bool', { nullable: true })
  deliveries?: boolean;

  @Column('bool', { nullable: true })
  deliveries_personalizated?: boolean;

  @Column('bool', { default: false })
  active: boolean;

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    select: false,
  })
  created_at: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    select: false,
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  updated_at: Date;

  @OneToOne(() => User, (user) => user.driver, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'userId' })
  user: User;
}
