import {
  IsBoolean,
  IsDateString,
  IsEmail,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsPositive,
  IsString,
  IsUUID,
  Matches,
  MaxLength,
  MinLength,
} from 'class-validator';

export class CreateDriverDto {
  // TODO: data for create user
  @IsString()
  @IsNotEmpty()
  @MinLength(1)
  @MaxLength(100)
  name: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(1)
  @MaxLength(100)
  last_name: string;

  @IsNotEmpty()
  ci_number: number;

  @IsString()
  @IsOptional()
  ci_extention?: string;

  @IsString()
  @IsNotEmpty()
  ci_expedition: string;

  @IsNotEmpty()
  cel_phone: number;

  @IsString()
  @IsNotEmpty()
  code_cel_phone: string;

  @IsNotEmpty()
  @IsDateString()
  date_birth: Date;

  @IsString()
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsString()
  @MinLength(6)
  @MaxLength(50)
  @Matches(/(?:(?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
    message:
      'The password must have a Uppercase, lowercase letter and a number',
  })
  password: string;

  // TODO: data for create driver

  @IsNotEmpty()
  // @IsInt()
  // @IsPositive()
  // @MinLength(1)
  movil_number: number;

  @IsNotEmpty()
  @IsString()
  license_number: string;

  @IsNotEmpty()
  @IsDateString()
  license_expiration_date: Date;

  @IsBoolean()
  @IsOptional()
  delivery_money?: boolean;

  @IsInt()
  @IsOptional()
  amount_delivery_money?: number;

  @IsBoolean()
  @IsOptional()
  deliveries?: boolean;

  @IsBoolean()
  @IsOptional()
  deliveries_personalizated?: boolean;
}
