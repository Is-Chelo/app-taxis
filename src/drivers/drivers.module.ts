import { Module } from '@nestjs/common';
import { DriversService } from './drivers.service';
import { DriversController } from './drivers.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Driver } from './entities/driver.entity';
import { UsersModule } from 'src/users/users.module';
import { PermissionsModule } from 'src/permissions/permissions.module';
import { RolesModule } from 'src/roles/roles.module';

@Module({
  controllers: [DriversController],
  providers: [DriversService],
  imports: [
    TypeOrmModule.forFeature([Driver]),
    UsersModule,
    UsersModule,
    PermissionsModule,
    RolesModule,
  ],
})
export class DriversModule {}
