interface rolePermission {
  select: boolean;
  create: boolean;
  update: boolean;
  delete: boolean;
}

interface SeedPermission {
  name: string;
  description: string;
  url: string;
  active: boolean;
  permissions: rolePermission;
}
interface SeedPermissionData {
  superadmin: SeedPermission[];
  admin: SeedPermission[];
  passenger: SeedPermission[];
  driver: SeedPermission[];
}
export const dataPermissions: SeedPermissionData = {
  superadmin: [
    {
      name: 'Module users',
      description: 'crud users',
      url: 'api/v1/users',
      active: true,
      permissions: {
        select: true,
        create: true,
        update: true,
        delete: true,
      },
    },
    {
      name: 'Module driver',
      description: 'crud users',
      url: 'api/v1/drivers',
      active: true,
      permissions: {
        select: true,
        create: true,
        update: true,
        delete: true,
      },
    },
    {
      name: 'Module vehicle brand',
      description: 'crud vehicle brand',
      url: 'api/v1/vehicle-brands',
      active: true,
      permissions: {
        select: true,
        create: true,
        update: true,
        delete: true,
      },
    },
    {
      name: 'Module vehicle Model',
      description: 'crud vehicle Model',
      url: 'api/v1/vehicle-models',
      active: true,
      permissions: {
        select: true,
        create: true,
        update: true,
        delete: true,
      },
    },
    {
      name: 'Module Services or Requests',
      description: 'crud Services or Requests',
      url: 'api/v1/type-requests',
      active: true,
      permissions: {
        select: true,
        create: true,
        update: true,
        delete: true,
      },
    },
    {
      name: 'Module Driver Vehicle',
      description: 'Module Driver Vehicle',
      url: 'api/v1/vehicles',
      active: true,
      permissions: {
        select: true,
        create: true,
        update: true,
        delete: true,
      },
    },
    {
      name: 'Module Organizations',
      description: 'Module Organizations',
      url: 'api/v1/organizations',
      active: true,
      permissions: {
        select: true,
        create: true,
        update: true,
        delete: true,
      },
    },
  ],

  admin: [
    {
      name: 'Module users',
      description: 'crud users',
      url: 'api/v1/users',
      active: true,
      permissions: {
        select: true,
        create: true,
        update: true,
        delete: true,
      },
    },
    {
      name: 'Module driver',
      description: 'crud users',
      url: 'api/v1/drivers',
      active: true,
      permissions: {
        select: true,
        create: true,
        update: true,
        delete: true,
      },
    },
    {
      name: 'Module vehicle brand',
      description: 'crud vehicle brand',
      url: 'api/v1/vehicle-brands',
      active: true,
      permissions: {
        select: true,
        create: true,
        update: true,
        delete: true,
      },
    },
    {
      name: 'Module vehicle Model',
      description: 'crud vehicle Model',
      url: 'api/v1/vehicle-models',
      active: true,
      permissions: {
        select: true,
        create: true,
        update: true,
        delete: true,
      },
    },
    {
      name: 'Module Services or Requests',
      description: 'Module Services or Requests',
      url: 'api/v1/type-requests',
      active: true,
      permissions: {
        select: true,
        create: false,
        update: false,
        delete: false,
      },
    },
    {
      name: 'Module Driver Vehicle',
      description: 'Module Driver Vehicle',
      url: 'api/v1/driver-vehicles',
      active: true,
      permissions: {
        select: true,
        create: true,
        update: true,
        delete: false,
      },
    },
    {
      name: 'Module Organizations',
      description: 'Module Organizations',
      url: 'api/v1/organizations',
      active: true,
      permissions: {
        select: true,
        create: false,
        update: true,
        delete: false,
      },
    },
  ],

  passenger: [
    {
      name: 'module users',
      description: 'crud users',
      url: 'api/v1/users',
      active: true,
      permissions: {
        select: true,
        create: false,
        update: true,
        delete: true,
      },
    },
    {
      name: 'module driver',
      description: 'crud users',
      url: 'api/v1/drivers',
      active: true,
      permissions: {
        select: true,
        create: false,
        update: false,
        delete: false,
      },
    },
    {
      name: 'module Services or Requests',
      description: 'crud Services or Requests',
      url: 'api/v1/type-requests',
      active: true,
      permissions: {
        select: true,
        create: false,
        update: false,
        delete: false,
      },
    },
    {
      name: 'Module Driver Vehicle',
      description: 'Module Driver Vehicle',
      url: 'api/v1/vehicles',
      active: true,
      permissions: {
        select: true,
        create: false,
        update: false,
        delete: false,
      },
    },
    {
      name: 'Module Organizations',
      description: 'Module Organizations',
      url: 'api/v1/organizations',
      active: true,
      permissions: {
        select: true,
        create: false,
        update: false,
        delete: false,
      },
    },
  ],

  driver: [
    {
      name: 'module users',
      description: 'crud users',
      url: 'api/v1/users',
      active: true,
      permissions: {
        select: true,
        create: false,
        update: false,
        delete: false,
      },
    },
    {
      name: 'module driver',
      description: 'crud users',
      url: 'api/v1/drivers',
      active: true,
      permissions: {
        select: true,
        create: false,
        update: false,
        delete: true,
      },
    },
    {
      name: 'module Services or Requests',
      description: 'crud Services or Requests',
      url: 'api/v1/type-requests',
      active: true,
      permissions: {
        select: true,
        create: false,
        update: false,
        delete: false,
      },
    },
    {
      name: 'Module Driver Vehicle',
      description: 'Module Driver Vehicle',
      url: 'api/v1/vehicles',
      active: true,
      permissions: {
        select: true,
        create: true,
        update: true,
        delete: false,
      },
    },
    {
      name: 'Module Organizations',
      description: 'Module Organizations',
      url: 'api/v1/organizations',
      active: true,
      permissions: {
        select: true,
        create: false,
        update: false,
        delete: false,
      },
    },
  ],
};
