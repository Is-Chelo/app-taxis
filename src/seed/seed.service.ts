import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Organization } from 'src/organizations/entities/organization.entity';
import { Permission } from 'src/permissions/entities/permission.entity';
import { Role } from 'src/roles/entities/role.entity';
import { RolePermission } from 'src/roles/entities/role_permissions.entity';
import { User } from 'src/users/entities/user.entity';
import { Repository } from 'typeorm';
import { dataPermissions } from './data/permissions';

import * as bcrypt from 'bcrypt';
import { brandsData } from './data/cars-details';
import { VehicleBrand } from 'src/vehicle-brands/entities/vehicle-brand.entity';
import { VehicleModel } from 'src/vehicle-models/entities/vehicle-model.entity';
import { TypeRequest } from 'src/type-requests/entities/type-request.entity';

@Injectable()
export class SeedService {
  private readonly logger = new Logger(SeedService.name);
  constructor(
    @InjectRepository(Role)
    private readonly roleRepository: Repository<Role>,

    @InjectRepository(Organization)
    private readonly organizationRepository: Repository<Organization>,

    @InjectRepository(Permission)
    private readonly permissionRepository: Repository<Permission>,

    @InjectRepository(RolePermission)
    private readonly rolePermissionRepository: Repository<RolePermission>,

    @InjectRepository(User)
    private readonly userRepository: Repository<User>,

    @InjectRepository(VehicleBrand)
    private readonly vehicleBrandRepository: Repository<VehicleBrand>,

    @InjectRepository(VehicleModel)
    private readonly vehicleModelRepository: Repository<VehicleModel>,

    @InjectRepository(TypeRequest)
    private readonly typeRequestRepository: Repository<TypeRequest>,
  ) {}

  async runAllSeeders() {
    let response = '';
    response += await this.seedOrganization();
    response += await this.seedPermissions();
    response += await this.seedRoles();
    response += await this.seedUserSuperAdmin();
    response += await this.seedUserAdmin();
    response += await this.seedUserPassenger();
    response += await this.seedVehicleBrands();
    response += await this.seedVehicleModels();
    response += await this.seedTypeRequest();

    return response;
  }

  async seedRoles() {
    await this.userRepository.delete({});
    await this.roleRepository.delete({});
    try {
      const response = await this.roleRepository
        .createQueryBuilder()
        .insert()
        .values([
          {
            id: '76f2d09c-3793-4dbf-bd63-69d8412bb7db',
            name: 'super-admin',
            description: 'Super Admin role',
          },
          {
            id: 'bee55883-d771-45dc-8c91-386da4425dec',
            name: 'admin',
            description: 'Admin role',
          },
          {
            id: 'e5b87883-7e23-4824-adeb-bd7a877f62d2',
            name: 'passenger',
            description: 'Passenger Role',
          },
          {
            id: '94c13352-62d4-477c-824e-e931c6569a8a',
            name: 'driver',
            description: 'Driver Role',
          },
        ])
        .execute();
      await this.seedRolePermissions(response.identifiers[0].id, 'superadmin');
      await this.seedRolePermissions(response.identifiers[1].id, 'admin');
      await this.seedRolePermissions(response.identifiers[2].id, 'passenger');
      await this.seedRolePermissions(response.identifiers[3].id, 'driver');
      return 'Roles seeded executed successfully\n';
    } catch (error) {
      this.handlerDbException(error);
    }
  }

  async seedOrganization() {
    await this.userRepository.delete({});
    await this.organizationRepository.delete({});
    try {
      this.organizationRepository
        .createQueryBuilder()
        .insert()
        .values([
          {
            organization_id: '300a05dc-0866-4c5a-8190-11808701d8b7',
            name: 'Solunes Organizacion de taxis',
            address: 'Calle 6 # 123',
            email: 'solunes@solunes.com',
            active: true,
            cell_phone_number: '59177112939',
            phone_number: '64-12939',
          },
        ])
        .execute();

      return 'Organization seeded executed successfully\n';
    } catch (error) {
      this.handlerDbException(error);
    }
  }

  async seedPermissions() {
    await this.rolePermissionRepository.delete({});
    await this.permissionRepository.delete({});
    try {
      await this.permissionRepository
        .createQueryBuilder()
        .insert()
        .values(dataPermissions.admin)
        .execute();
      return 'Permissions seeded executed successfully\n';
    } catch (error) {
      this.handlerDbException(error);
    }
  }

  async seedRolePermissions(id: string, role: string) {
    const roleDB = await this.roleRepository.findOne({ where: { id: id } });
    try {
      for (let i = 0; i < dataPermissions[role].length; i++) {
        const permission = await this.permissionRepository.findOne({
          where: { name: dataPermissions[role][i].name },
        });
        if (!permission)
          throw new InternalServerErrorException(
            `Permission with name ${dataPermissions[role][i].name} not found in database (Permission)`,
          );
        const rolePermission = this.rolePermissionRepository.create({
          role: roleDB,
          permission: permission,
          ...dataPermissions[role][i].permissions,
        });
        await this.rolePermissionRepository.save(rolePermission);
      }
    } catch (error) {}
  }

  async seedVehicleBrands() {
    await this.vehicleModelRepository.delete({});
    await this.vehicleBrandRepository.delete({});
    try {
      for (let i = 0; i < brandsData.brands.length; i++) {
        await this.vehicleBrandRepository.query(
          `INSERT INTO vehicle_brands (brand_id, name, active) values (${brandsData.brands[i].brand_id}, '${brandsData.brands[i].name}', true)`,
        );
      }
      return 'Vehicle Brands seeded executed successfully\n';
    } catch (error) {
      console.log(error);
    }
  }

  async seedVehicleModels() {
    try {
      for (let i = 0; i < brandsData.models.length; i++) {
        const vehicleBrand = await this.vehicleBrandRepository.findOne({
          where: { brand_id: brandsData.models[i].vehicle_brandId },
        });
        const model = this.vehicleModelRepository.create({
          name: brandsData.models[i].name,
          vehicle_brand: vehicleBrand,
        });
        await this.vehicleModelRepository.save(model);
      }

      return 'Vehicle Models seeded executed successfully\n';
    } catch (error) {
      console.log(error);
    }
  }

  async seedUserSuperAdmin() {
    await this.userRepository.delete({});
    try {
      const role = await this.roleRepository.findOne({
        where: { name: 'super-admin' },
      });

      const user = this.userRepository.create({
        name: 'Super Admin',
        last_name: 'Super Admin',
        cel_phone: 123456,
        ci_number: 123456,
        ci_expedition: 'CH',
        date_birth: '1999-05-28',
        password: bcrypt.hashSync('Super123', 10),
        email: 'super@gmail.com',
        code_cel_phone: '+591',
        is_verified: true,
        role,
      });

      await this.userRepository.save(user);

      return 'User Super Admin seeded executed successfully\n';
    } catch (error) {
      this.handlerDbException(error);
    }
  }

  async seedUserAdmin() {
    try {
      const role = await this.roleRepository.findOne({
        where: { name: 'admin' },
      });
      const organization = await this.organizationRepository.findOne({
        where: { organization_id: '300a05dc-0866-4c5a-8190-11808701d8b7' },
      });
      const user = this.userRepository.create({
        name: 'Solunes Taxis Admin',
        last_name: 'Solunes Taxis',
        cel_phone: 123456,
        ci_number: 123456,
        ci_expedition: 'CH',
        date_birth: '1999-05-28',
        password: bcrypt.hashSync('Admin123', 10),
        email: 'adminsolunes@gmail.com',
        code_cel_phone: '+591',
        is_verified: true,
        role,
        organization,
      });
      await this.userRepository.save(user);

      return 'User Admin seeded executed successfully\n';
    } catch (error) {
      this.handlerDbException(error);
    }
  }

  async seedUserPassenger() {
    try {
      const role = await this.roleRepository.findOne({
        where: { name: 'passenger' },
      });
      const user = this.userRepository.create({
        name: 'Solunes Pasajero',
        last_name: 'Pasajero',
        cel_phone: 123456,
        ci_number: 123456,
        ci_expedition: 'CH',
        date_birth: '1999-05-28',
        password: bcrypt.hashSync('Pasajero123', 10),
        email: 'pasajero@gmail.com',
        code_cel_phone: '+591',
        is_verified: true,
        role,
      });

      await this.userRepository.save(user);

      return 'User Passenger seeded executed successfully\n';
    } catch (error) {
      this.handlerDbException(error);
    }
  }

  async seedTypeRequest() {
    await this.typeRequestRepository.delete({});
    try {
      await this.typeRequestRepository
        .createQueryBuilder()
        .insert()
        .values([
          {
            name: 'Viajes',
            nameForFront: 'Viajes',
            description:
              'Una de las formas más rápidas para ir de un punto a otro. Elige el destino y pon el precio.',
            logo: 'https://imagen.com',
            active: true,
          },
          {
            name: 'Solicitar Dinero',
            nameForFront: 'Solicitar Dinero',
            description:
              '¿Necesitas dinero en efectivo? No te preocupes, nosotros nos encargamos.  La carrera se paga por anticipado',
            logo: 'https://imagen.com',
            active: true,
          },
          {
            name: 'Envíos',
            nameForFront: 'Envíos',
            description:
              '¿Necesitas enviar algo frágil? No te preocupes, nosotros nos encargamos. La carrera se paga por anticipado',
            logo: 'https://imagen.com',
            active: true,
          },
          {
            name: 'Pedidos personalizados',
            nameForFront: 'Pedidos personalizados',
            description:
              '¿Quieres pedir algo? Solo dinos que es lo que necesitas y nosotros te lo traemos. La carrera se paga por anticipado',
            logo: 'https://imagen.com',
            active: true,
          },
        ])
        .execute();
      return 'Services seeded executed successfully\n';
    } catch (error) {
      console.log(error);
    }
  }

  private handlerDbException(error: any) {
    this.logger.error(error);
    if (error.code === '23505') throw new BadRequestException(error.detail);

    if (error.code === '23502')
      throw new BadRequestException(error.column + ' is required');

    throw new InternalServerErrorException();
  }
}
