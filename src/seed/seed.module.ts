import { Module } from '@nestjs/common';
import { SeedService } from './seed.service';
import { SeedController } from './seed.controller';
import { RolesModule } from 'src/roles/roles.module';
import { OrganizationsModule } from 'src/organizations/organizations.module';
import { PermissionsModule } from 'src/permissions/permissions.module';
import { UsersModule } from 'src/users/users.module';
import { VehicleBrandsModule } from 'src/vehicle-brands/vehicle-brands.module';
import { VehicleModelsModule } from 'src/vehicle-models/vehicle-models.module';
import { TypeRequestsModule } from 'src/type-requests/type-requests.module';

@Module({
  controllers: [SeedController],
  providers: [SeedService],
  imports: [
    RolesModule,
    OrganizationsModule,
    PermissionsModule,
    UsersModule,
    VehicleBrandsModule,
    VehicleModelsModule,
    TypeRequestsModule,
  ],
})
export class SeedModule {}
