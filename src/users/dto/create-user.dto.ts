import { ApiProperty } from '@nestjs/swagger';
import {
  IsDateString,
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsString,
  // IsUUID,
  Matches,
  MaxLength,
  MinLength,
} from 'class-validator';

export class CreateUserDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @MinLength(1)
  @MaxLength(100)
  name: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @MinLength(1)
  @MaxLength(100)
  last_name: string;

  @ApiProperty()
  @IsNotEmpty()
  ci_number: number;

  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  ci_extention?: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  ci_expedition: string;

  @ApiProperty()
  @IsNotEmpty()
  cel_phone: number;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  code_cel_phone: string;

  @ApiProperty({
    example: '2021-01-01',
    description: 'User date birth',
  })
  @IsNotEmpty()
  @IsDateString()
  date_birth: Date;

  @ApiProperty()
  @IsString()
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @ApiProperty()
  @IsString()
  @MinLength(6)
  @MaxLength(50)
  @Matches(/(?:(?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
    message:
      'The password must have a Uppercase, lowercase letter and a number',
  })
  password: string;

  @ApiProperty({ type: 'string', format: 'binary', required: false })
  @IsOptional()
  photo?: string;
}
