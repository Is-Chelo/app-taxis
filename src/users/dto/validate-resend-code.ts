import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty, IsString } from 'class-validator';

export class validateResendCodeDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  code_cel_phone: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsInt()
  cel_phone: number;
}
