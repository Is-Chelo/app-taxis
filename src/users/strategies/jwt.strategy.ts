import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { InjectRepository } from '@nestjs/typeorm';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { Repository } from 'typeorm';
import { User } from '../entities/user.entity';
import { JwtPayload } from '../interfaces/JwtPayload';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_SECRET,
    });
  }

  async validate(payload: JwtPayload) {
    const user = await this.usersRepository.findOne({
      where: { user_id: payload.id },
      relations: {
        role: true,
        organization: true,
      },
    });

    if (!user)
      throw new UnauthorizedException({
        status: false,
        message: 'Token invalid',
        error: 'Unauthorized',
      });
    if (!user.is_verified)
      throw new UnauthorizedException({
        status: false,
        message: 'User not verified',
        error: 'Unauthorized',
      });
    return user;
  }
}
