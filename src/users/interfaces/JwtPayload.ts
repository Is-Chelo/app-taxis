export interface JwtPayload {
  id: string;
  name: string;
  last_name: string;
  cel_phone: number;
  code_cel_phone: string;
}
