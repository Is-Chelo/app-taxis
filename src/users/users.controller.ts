import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  ParseUUIDPipe,
  Query,
  UseInterceptors,
  UploadedFile,
  Res,
} from '@nestjs/common';
import { Response } from 'express';

import { UsersService } from './users.service';
import { ValidateOtpDto } from './dto/validate-otp.dto';
import { PaginationDto } from 'src/common/dtos/pagination.dto';
import { validateResendCodeDto } from './dto/validate-resend-code';
import { Auth } from './decorators/auth.decorator';
import { loginDto } from './dto/login-user.dto';
import { CreateUserDto } from './dto/create-user.dto';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiConsumes,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger/dist/decorators';
import { CreateUserEmailDto } from './dto/create-user-email';
import { FileInterceptor } from '@nestjs/platform-express';
import { fileFilter } from 'src/common/helpers/fileFilter.helper';
import { diskStorage } from 'multer';
import { fileNamer } from 'src/common/helpers/fileNamer.helper';
import {
  getUser,
  loginSchema,
  resendCodeSchema,
  userCreateEmailSchema,
  userCreateSchema,
} from './swagger/user-schemas';
import {
  badRequestException,
  notFoundException,
  unauthorizedException,
} from 'src/common/swagger/schemas';

@ApiTags('Auth')
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @ApiCreatedResponse({
    schema: userCreateSchema,
    description: 'User created successfully',
  })
  @ApiBadRequestResponse({
    schema: badRequestException,
    description: 'Bad request',
  })
  @ApiConsumes('multipart/form-data')
  @Post('sing-up')
  @UseInterceptors(
    FileInterceptor('photo', {
      fileFilter: fileFilter,
      storage: diskStorage({
        destination: './static/users',
        filename: fileNamer,
      }),
    }),
  )
  registerUser(
    @UploadedFile() file: Express.Multer.File,
    @Body() createUserDto: CreateUserDto,
  ) {
    return this.usersService.create(createUserDto, file);
  }

  @ApiCreatedResponse({
    schema: userCreateEmailSchema,
    description: 'User created successfully',
  })
  @ApiBadRequestResponse({
    schema: badRequestException,
    description: 'Bad request',
  })
  @Post('sing-in/email')
  registerWithEmail(@Body() createUserEmailDto: CreateUserEmailDto) {
    return this.usersService.createUserWithEmail(createUserEmailDto);
  }

  @ApiNotFoundResponse({
    schema: notFoundException,
    description: 'Not found',
  })
  @ApiBadRequestResponse({
    schema: badRequestException,
    description: 'Bad request',
  })
  @ApiOkResponse({
    schema: userCreateEmailSchema,
    description: 'User validated successfully',
  })
  @Post('validate-otp')
  validateOTP(@Body() validateOtpDto: ValidateOtpDto) {
    return this.usersService.validateOTP(validateOtpDto);
  }

  @ApiNotFoundResponse({
    schema: notFoundException,
    description: 'Not found',
  })
  @ApiBadRequestResponse({
    schema: badRequestException,
    description: 'Bad request',
  })
  @ApiOkResponse({
    schema: loginSchema,
    description: 'User logged in',
  })
  @Post('sing-in')
  loginPassenger(@Body() loginUserDto: loginDto) {
    return this.usersService.login(loginUserDto);
  }

  @ApiNotFoundResponse({
    schema: notFoundException,
    description: 'Not found',
  })
  @ApiBadRequestResponse({
    schema: badRequestException,
    description: 'Bad request',
  })
  @ApiOkResponse({
    schema: resendCodeSchema,
    description: 'Verify OTP',
  })
  @Post('resend-code')
  resendCode(@Body() validateResendCode: validateResendCodeDto) {
    return this.usersService.resendCode(validateResendCode);
  }

  @ApiOkResponse({
    schema: {
      type: 'string',
      format: 'binary',
    },
    description: 'Get Image for user',
  })
  @ApiBadRequestResponse({
    schema: badRequestException,
    description: 'Bad request',
  })
  @Get('image/:photo')
  getPhotoUser(@Res() res: Response, @Param('photo') photo: string) {
    const path = this.usersService.getPhotoUser(photo);
    res.sendFile(path);
  }

  @ApiBearerAuth('defaultBearerAuth')
  @ApiOkResponse({
    schema: {
      type: 'array',
      example: [getUser, getUser],
    },
    description: 'Get All users',
  })
  @ApiUnauthorizedResponse({
    schema: unauthorizedException,
    description: 'Unauthorized',
  })
  @Get()
  @Auth()
  findAll(@Query() paginationDto: PaginationDto) {
    return this.usersService.findAll(paginationDto);
  }

  @ApiBearerAuth('defaultBearerAuth')
  @ApiOkResponse({
    schema: {
      type: 'object',
      example: {
        status: 200,
        message: 'success',
        data: getUser,
      },
    },
    description: 'Get One users',
  })
  @ApiNotFoundResponse({
    schema: notFoundException,
    description: 'Not found',
  })
  @ApiUnauthorizedResponse({
    schema: unauthorizedException,
    description: 'Unauthorized',
  })
  @Get(':id')
  @Auth()
  findOne(@Param('id', ParseUUIDPipe) id: string) {
    return this.usersService.findOne(id);
  }

  @ApiBearerAuth('defaultBearerAuth')
  @ApiOkResponse({
    schema: {
      type: 'object',
      example: {
        status: true,
        message: 'User deleted successfully',
      },
    },
    description: 'User Delete',
  })
  @ApiNotFoundResponse({
    schema: notFoundException,
    description: 'Not found',
  })
  @ApiUnauthorizedResponse({
    schema: unauthorizedException,
    description: 'Unauthorized',
  })
  @Delete(':id')
  @Auth()
  remove(@Param('id') id: string) {
    return this.usersService.remove(id);
  }
}
