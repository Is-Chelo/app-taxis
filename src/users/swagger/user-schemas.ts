export const userCreateSchema = {
  type: 'object',
  example: {
    status: true,
    message: 'User created successfully',
    data: {
      code: '123',
      user: {
        id: '44fea50f-31fd-4003-bc14-56ec7144a369',
        name: 'Juan',
        last_name: 'Perez',
        ci_number: 123456,
        email: 'prueba@gmail.com',
        ci_expedition: 'LP',
        ci_extention: '',
        cel_phone: 77665544,
        date_birth: '200-03-12',
        code_cel_phone: '591',
        is_verified: false,
        role: 'passenger',
        photo: 'htpp://localhost:3000/users/image/1.png',
      },
    },
  },
};
export const userCreateEmailSchema = {
  type: 'object',
  example: {
    status: true,
    message: 'User created successfully',
    data: {
      token: 'skesdfvoo232klvcskdfv',
      id: '44fea50f-31fd-4003-bc14-56ec7144a369',
      name: 'Juan',
      last_name: 'Perez',
      cel_phone: 77665544,
      code_cel_phone: '591',
      is_verified: false,
      role: 'passenger',
      photo: 'htpp://localhost:3000/users/image/1.png',
    },
  },
};
export const resendCodeSchema = {
  type: 'object',
  example: {
    status: true,
    message: 'Vefiry OTP',
    data: {
      code: '123456',
      user_id: '44fea50f-31fd-4003-bc14-56ec7144a369',
      cel_phone: 77665544,
      code_cel_phone: '591',
    },
  },
};
export const loginSchema = {
  type: 'object',
  example: {
    status: true,
    message: 'User successfully logged in',
    data: {
      token: 'xskesdfvoo232klvcskdfv',
      id: '44fea50f-31fd-4003-bc14-56ec7144a369',
      name: 'Juan',
      last_name: 'Perez',
      cel_phone: 77665544,
      code_cel_phone: '591',
      role: 'passenger',
    },
  },
};

export const getUser = {
  user_id: '55797069-28e6-4c0c-83a8-fa0036fdfc2a',
  name: 'prueba',
  last_name: 'prueba',
  ci_number: 10239,
  ci_extention: '    ',
  ci_expedition: 'CH',
  cel_phone: 77112939,
  date_birth: '2021-01-01',
  email: 'prueba@gmail.com',
  code_cel_phone: '591',
  is_verified: false,
  photo:
    'http://localhost:3000/api/v1/users/image/3dfb650c-cff9-4313-91d2-2e062bd4eafe.jpeg',
  role: {
    id: 'e5b87883-7e23-4824-adeb-bd7a877f62d2',
    name: 'passenger',
    description: 'Passenger Role',
  },
};
