export enum Roles {
  ADMIN = 'admin',
  DRIVER = 'driver',
  PASSENGER = 'passenger',
}
