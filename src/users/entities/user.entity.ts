import { ApiProperty } from '@nestjs/swagger';
import { Driver } from 'src/drivers/entities/driver.entity';
import { Organization } from 'src/organizations/entities/organization.entity';
import { Role } from 'src/roles/entities/role.entity';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Opt } from './opt.entity';

@Entity('users')
export class User {
  @ApiProperty({
    example: 'f9d0a31b-7596-4011-94d5-7a200318ac9d',
    description: 'User id',
    uniqueItems: true,
    name: 'id',
  })
  @PrimaryGeneratedColumn('uuid')
  user_id: string;

  @ApiProperty({
    example: 'Marcelo',
    description: 'User name',
    nullable: false,
  })
  @Column('varchar', { length: 100, nullable: false })
  name: string;

  @ApiProperty({
    example: 'Torres',
    description: 'User last name',
    nullable: false,
  })
  @Column('varchar', { length: 100, nullable: false })
  last_name: string;

  @ApiProperty({
    example: 10342310,
    description: 'User ci number',
    nullable: true,
  })
  @Column('int', { nullable: true })
  ci_number: number;

  @ApiProperty({
    example: 'A',
    description: 'User ci extention',
    nullable: true,
  })
  @Column('char', { length: 4, nullable: true })
  ci_extention?: string;

  @ApiProperty({
    example: 'CH',
    description: 'User ci expedition',
    nullable: true,
  })
  @Column('char', { length: 2, nullable: true })
  ci_expedition: string;

  @ApiProperty({
    example: 75654345,
    description: 'User cel phone',
    nullable: true,
  })
  @Column('int', { nullable: true })
  cel_phone?: number;

  @ApiProperty({
    example: '2000-01-01',
    description: 'User date birth',
    nullable: true,
  })
  @Column('date', { nullable: true })
  date_birth?: Date;

  @Column('boolean', { default: false, select: false })
  google: boolean;

  @Column('boolean', { default: false, select: false })
  facebook: boolean;

  @ApiProperty({
    example: 'chelo@gmail.com',
    description: 'User email',
    nullable: false,
  })
  @Column('varchar', { length: 100, nullable: false })
  email: string;

  @Column('varchar', { length: 64, nullable: false, select: false })
  password: string;

  @ApiProperty({
    example: '+591',
    description: 'User code cel phone',
    nullable: true,
  })
  @Column('varchar', { length: 4, nullable: true })
  code_cel_phone?: string;

  @ApiProperty({
    example: true,
    description: 'User verified',
  })
  @Column('boolean', { default: false })
  is_verified: boolean;

  @ApiProperty({
    example:
      'http://localhost:3000/api/v1/users/image/bb1777cd-25ee-4186-a3ca-11f5450d9df5.jpeg',
    description: 'User code cel phone',
    nullable: true,
  })
  @Column('varchar', { nullable: true })
  photo: string;

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    select: false,
  })
  created_at: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    select: false,
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  updated_at: Date;

  @OneToMany(() => Opt, (opt) => opt.user)
  opts: Opt[];

  @ManyToOne(() => Role, (role) => role.users, { eager: true })
  role: Role;

  @OneToOne(() => Driver, (driver) => driver.user, { onDelete: 'CASCADE' })
  driver: Driver;

  @ManyToOne(() => Organization, (organization) => organization.users)
  organization?: Organization;

  @BeforeInsert()
  checkFieldsBeforeInsert() {
    this.email = this.email.toLowerCase().trim();
  }

  @BeforeUpdate()
  checkFieldsBeforeUpdate() {
    this.checkFieldsBeforeInsert();
  }
}
