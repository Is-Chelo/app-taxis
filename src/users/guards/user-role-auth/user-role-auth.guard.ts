import {
  BadRequestException,
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Observable } from 'rxjs';
import { Permission } from 'src/permissions/entities/permission.entity';
import { RolePermission } from 'src/roles/entities/role_permissions.entity';
import { Repository } from 'typeorm';

@Injectable()
export class UserRoleAuthGuard implements CanActivate {
  constructor(
    @InjectRepository(Permission)
    private readonly permissionRepository: Repository<Permission>,

    @InjectRepository(RolePermission)
    private readonly rolePermissionRepository: Repository<RolePermission>,
  ) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();

    const originalUrl = request.originalUrl;

    let baseUrl = originalUrl.split('/');

    baseUrl = `${baseUrl[1]}/${baseUrl[2]}/${baseUrl[3].split('?')[0]}`;
    // TODO: get permission from database
    return this.getPermissionFromDatabase(
      baseUrl,
      request.method,
      request.user.role.id,
    );
  }

  private async getPermissionFromDatabase(
    baseUrl: string,
    method: string,
    roleID: string,
  ) {
    const permission = await this.permissionRepository.findOneBy({
      url: baseUrl,
    });
    if (!permission)
      throw new BadRequestException({
        status: false,
        message: `Services ${baseUrl} not found in database`,
        error: 'Bad Request',
      });
    const rolePermission = await this.rolePermissionRepository.findOne({
      where: {
        permission: {
          id: permission.id,
        },
        role: {
          id: roleID,
        },
      },
    });
    if (!rolePermission)
      throw new BadRequestException({
        status: false,
        message: 'This Role have not permission',
        error: 'Bad Request',
      });
    switch (method) {
      case 'PATCH':
        if (rolePermission.update) return true;
        else
          throw new UnauthorizedException({
            status: false,
            message: 'You have not permission',
            error: 'Unauthorized',
          });
      case 'GET':
        if (rolePermission.select) return true;
        else
          throw new UnauthorizedException({
            status: false,
            message: 'You have not permission',
            error: 'Unauthorized',
          });
      case 'POST':
        if (rolePermission.create) return true;
        else
          throw new UnauthorizedException({
            status: false,
            message: 'You have not permission',
            error: 'Unauthorized',
          });
      case 'DELETE':
        if (rolePermission.delete) return true;
        else
          throw new UnauthorizedException({
            status: false,
            message: 'You have not permission',
            error: 'Unauthorized',
          });
    }
  }
}
