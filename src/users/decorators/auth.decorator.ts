import { applyDecorators, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UserRoleAuthGuard } from '../guards/user-role-auth/user-role-auth.guard';

export function Auth() {
  return applyDecorators(UseGuards(AuthGuard(), UserRoleAuthGuard));
}
