import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { PaginationDto } from 'src/common/dtos/pagination.dto';
import { Role } from 'src/roles/entities/role.entity';
import { Repository } from 'typeorm';
import { loginDto } from './dto/login-user.dto';
import { ValidateOtpDto } from './dto/validate-otp.dto';
import { validateResendCodeDto } from './dto/validate-resend-code';
import { Opt } from './entities/opt.entity';
import { User } from './entities/user.entity';
import { JwtPayload } from './interfaces/JwtPayload';

import * as bcrypt from 'bcrypt';
import { CreateUserDto } from './dto/create-user.dto';
import { handlerDbException } from 'src/common/helpers/handlerDbException';
import { CreateUserEmailDto } from './dto/create-user-email';
import { Roles } from './enums/roles.enum';
import { join } from 'path';
import { existsSync } from 'fs';
@Injectable()
export class UsersService {
  private readonly logger = new Logger(UsersService.name);
  constructor(
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
    @InjectRepository(Opt)
    private readonly optRepository: Repository<Opt>,
    @InjectRepository(Role)
    private readonly roleRepository: Repository<Role>,

    private readonly jwtService: JwtService,
  ) {}

  // * user register
  async create(createUserDto: CreateUserDto, file: Express.Multer.File) {
    const { password, email, ci_number, cel_phone, ...user } = createUserDto;
    // TODO: validate if user exists
    await this.validateUserExists(email, ci_number, cel_phone);

    const role = await this.roleRepository.findOne({
      where: { name: Roles.PASSENGER },
    });
    const userCreated = this.usersRepository.create({
      ...user,
      password: await bcrypt.hash(password, 10),
      email,
      ci_number,
      cel_phone,
      role,
      photo: file ? `${process.env.HOST_URL}/users/image/${file.filename}` : '',
    });

    try {
      const user = await this.usersRepository.save(userCreated);

      const otpCode = this.generateOTP();
      await this.optRepository.save({
        code: otpCode,
        time_expiration_code: new Date().getTime() + 1000 * 60 * 60, // 1 hour
        user,
      });

      // TODO: send code to cel phone or email

      return {
        status: true,
        message: 'User created successfully, please validate your code',
        data: {
          code: otpCode,
          user: {
            id: user.user_id,
            name: user.name,
            last_name: user.last_name,
            ci_number: user.ci_number,
            email: user.email,
            ci_expedition: user.ci_expedition,
            ci_extention: user.ci_extention,
            cel_phone: user.cel_phone,
            date_birth: user.date_birth,
            code_cel_phone: user.code_cel_phone,
            is_verified: user.is_verified,
            role: user.role.name,
            photo: user.photo,
          },
        },
      };
    } catch (error) {
      handlerDbException(error, this.logger);
    }
  }

  // * validate if user exists
  async validateUserExists(
    email: string,
    ci_number: number,
    cel_phone: number,
  ) {
    const user = await this.usersRepository.findOne({
      where: [{ email }, { ci_number }, { cel_phone }],
    });
    if (user) {
      throw new BadRequestException({
        status: false,
        message: 'User with credentials already exists',
        error: 'Bad Request',
      });
    }
  }

  // * user validate OTP
  async validateOTP(validateOTP: ValidateOtpDto) {
    const { otp, user_id } = validateOTP;
    const currentTime = new Date().getTime();
    // TODO: validate if user exists
    await this.findOne(user_id);
    const codeOTP = await this.optRepository.findOne({
      where: {
        code: otp,
        user: {
          user_id: user_id,
        },
      },
      relations: {
        user: true,
      },
    });

    if (!codeOTP)
      throw new NotFoundException({
        status: false,
        message: 'OTP code not found',
        error: 'Not Found',
      });

    if (currentTime > codeOTP.time_expiration_code)
      throw new BadRequestException({
        status: false,
        message: 'OTP code expired, please generate a new one',
        error: 'Bad Request',
      });

    // TODO: update user to active
    const userActive = await this.usersRepository.preload({
      user_id: user_id,
      is_verified: true,
    });

    try {
      await this.usersRepository.save(userActive);
      // TODO: token JWT
      const token = this.generateJwtToken({
        id: userActive.user_id,
        name: userActive.name,
        last_name: userActive.last_name,
        cel_phone: userActive.cel_phone,
        code_cel_phone: userActive.code_cel_phone,
      });

      return {
        status: true,
        message: 'User verified',
        data: {
          token,
          id: userActive.user_id,
          name: userActive.name,
          last_name: userActive.last_name,
          cel_phone: userActive.cel_phone,
          code_cel_phone: userActive.code_cel_phone,
          is_verified: userActive.is_verified,
          role: codeOTP.user.role,
          photo: userActive.photo,
        },
      };
    } catch (error) {
      console.log(error);
      this.logger.error(error);
      throw new InternalServerErrorException({
        status: false,
        message: 'Internal Server Error',
        error: 'Internal Server Error',
      });
    }
  }

  async login(loginDto: loginDto) {
    const { email, password } = loginDto;
    const user = await this.usersRepository.findOne({
      where: {
        email,
      },
      relations: {
        role: true,
      },
      select: {
        user_id: true,
        name: true,
        last_name: true,
        cel_phone: true,
        code_cel_phone: true,
        is_verified: true,
        password: true,
        photo: true,
      },
    });
    if (!user)
      throw new NotFoundException({
        status: false,
        message: `User with email: ${email} not found, please register`,
        error: 'Not Found',
      });

    const passwordMatch = await bcrypt.compare(password, user.password);
    if (!passwordMatch)
      throw new BadRequestException({
        status: false,
        message: 'Invalid credentials',
        error: 'Bad Request',
      });

    if (!user.is_verified)
      throw new BadRequestException({
        status: false,
        message: 'User not verified, please verify your account',
        error: 'Bad Request',
      });

    const token = this.generateJwtToken({
      id: user.user_id,
      name: user.name,
      last_name: user.last_name,
      cel_phone: user.cel_phone,
      code_cel_phone: user.code_cel_phone,
    });

    return {
      status: true,
      message: 'User successfully logged in',
      data: {
        token,
        id: user.user_id,
        name: user.name,
        last_name: user.last_name,
        cel_phone: user.cel_phone,
        code_cel_phone: user.code_cel_phone,
        role: user.role.name,
      },
    };
  }

  async resendCode(resendCodeDto: validateResendCodeDto) {
    const { cel_phone, code_cel_phone } = resendCodeDto;
    const otpCode = this.generateOTP();
    const user = await this.usersRepository.findOneBy({
      cel_phone,
      code_cel_phone,
    });

    if (!user)
      throw new NotFoundException({
        status: false,
        message: `User with number phone: ${cel_phone} not found, please register`,
        error: 'Not Found',
      });
    try {
      await this.optRepository.save({
        code: otpCode,
        time_expiration_code: new Date().getTime() + 1000 * 60 * 60, // 1 hour
        user,
      });
    } catch (error) {
      handlerDbException(error, this.logger);
    }

    // TODO: Send SMS with OTP

    return {
      status: true,
      message: 'Verify OTP',
      data: {
        code: otpCode,
        user_id: user.user_id,
        cel_phone,
        code_cel_phone,
      },
    };
  }

  async findAll(paginationDto: PaginationDto) {
    const users = this.usersRepository.find({
      skip: paginationDto.offset,
      take: paginationDto.limit,
    });
    return {
      status: true,
      message: 'success',
      data: users,
    };
  }

  async findOne(id: string) {
    const user = await this.usersRepository.findOneBy({ user_id: id });
    if (!user)
      throw new NotFoundException({
        status: false,
        message: `User with id: ${id} not found`,
        error: 'Not found',
      });
    return {
      status: true,
      message: 'success',
      data: user,
    };
  }

  async createUserWithEmail(createUserEmailDto: CreateUserEmailDto) {
    const {
      displayName,
      email,
      phoneNumber = '',
      photoURL = '',
    } = createUserEmailDto;

    let user = await this.usersRepository.findOneBy({ email });

    if (!user) {
      const role = await this.roleRepository.findOneBy({
        name: Roles.PASSENGER,
      });

      // TODO: create user
      const userCreate = this.usersRepository.create({
        name: displayName,
        last_name: '',
        email,
        cel_phone: +phoneNumber,
        photo: photoURL,
        google: true,
        is_verified: true,
        password: ':P',
        role,
      });
      try {
        user = await this.usersRepository.save(userCreate);
      } catch (error) {
        console.log(error);
      }
    }

    const token = this.generateJwtToken({
      id: user.user_id,
      name: user.name,
      last_name: user.last_name,
      cel_phone: user.cel_phone,
      code_cel_phone: user.code_cel_phone,
    });

    return {
      status: true,
      message: 'User created successfully',
      data: {
        token,
        id: user.user_id,
        name: user.name,
        last_name: user.last_name,
        cel_phone: user.cel_phone,
        code_cel_phone: user.code_cel_phone,
        is_verified: user.is_verified,
        role: user.role.name,
        photo: user.photo,
      },
    };
  }

  async remove(id: string) {
    const user = await this.findOne(id);
    await this.usersRepository.remove(user.data);
    return {
      status: true,
      message: 'User deleted successfully',
    };
  }

  // TODO: Generate One Time Password (OTP)
  private generateOTP(otp_length = 6) {
    const digits = '0123456789';
    let OTP = '';
    for (let i = 0; i < otp_length; i++) {
      OTP += digits[Math.floor(Math.random() * 10)];
    }
    return OTP;
  }

  private generateJwtToken(payload: JwtPayload) {
    const accessToken = this.jwtService.sign(payload);
    return accessToken;
  }

  // TODO: PHOTO services
  getPhotoUser(photo: string) {
    const path = join(__dirname, '../../static/users', photo);
    if (!existsSync(path))
      throw new BadRequestException({
        status: false,
        message: 'Photo not found',
        error: 'Bad request',
      });
    return path;
  }
}
