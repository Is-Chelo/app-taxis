import { Module } from '@nestjs/common';
import { VehicleBrandsService } from './vehicle-brands.service';
import { VehicleBrandsController } from './vehicle-brands.controller';
import { VehicleBrand } from './entities/vehicle-brand.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from 'src/users/users.module';
import { PermissionsModule } from 'src/permissions/permissions.module';
import { RolesModule } from 'src/roles/roles.module';

@Module({
  controllers: [VehicleBrandsController],
  providers: [VehicleBrandsService],
  imports: [
    TypeOrmModule.forFeature([VehicleBrand]),
    UsersModule,
    PermissionsModule,
    RolesModule,
  ],
  exports: [TypeOrmModule, VehicleBrandsService],
})
export class VehicleBrandsModule {}
