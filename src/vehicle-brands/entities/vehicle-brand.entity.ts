import { VehicleModel } from 'src/vehicle-models/entities/vehicle-model.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('vehicle_brands')
export class VehicleBrand {
  @PrimaryGeneratedColumn()
  brand_id: string;

  @Column('varchar', { length: 100, nullable: false, unique: true })
  name: string;

  @Column('bool', { default: true })
  active: boolean;

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    select: false,
  })
  created_at: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    select: false,
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  updated_at: Date;

  @OneToMany(() => VehicleModel, (vehicle_model) => vehicle_model.vehicle_brand)
  vehicle_models: VehicleModel[];
}
