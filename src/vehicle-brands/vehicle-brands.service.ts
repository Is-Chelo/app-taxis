import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PaginationDto } from 'src/common/dtos/pagination.dto';
import { Repository } from 'typeorm';
import { CreateVehicleBrandDto } from './dto/create-vehicle-brand.dto';
import { UpdateVehicleBrandDto } from './dto/update-vehicle-brand.dto';
import { VehicleBrand } from './entities/vehicle-brand.entity';

@Injectable()
export class VehicleBrandsService {
  private readonly logger = new Logger(VehicleBrandsService.name);
  constructor(
    @InjectRepository(VehicleBrand)
    private readonly vehicleBrandRepository: Repository<VehicleBrand>,
  ) {}
  async create(createVehicleBrandDto: CreateVehicleBrandDto) {
    const vehicleBrand = this.vehicleBrandRepository.create(
      createVehicleBrandDto,
    );
    try {
      await this.vehicleBrandRepository.save(vehicleBrand);

      return {
        message: 'Vehicle Brand saved successfully',
        data: vehicleBrand,
      };
    } catch (error) {
      this.handlerDbException(error);
    }
  }

  findAll(paginationDto: PaginationDto) {
    return this.vehicleBrandRepository.find({
      skip: paginationDto.offset,
      take: paginationDto.limit,
    });
  }

  async findOne(id: string) {
    const vehicleBrand = await this.vehicleBrandRepository.findOneBy({
      brand_id: id,
    });
    if (!vehicleBrand)
      throw new NotFoundException(`Vehicle Brand with id: ${id} not found`);
    return {
      message: 'success',
      data: vehicleBrand,
    };
  }

  async update(id: string, updateVehicleBrandDto: UpdateVehicleBrandDto) {
    const vehicleBrands = await this.vehicleBrandRepository.preload({
      brand_id: id,
      ...updateVehicleBrandDto,
    });
    if (!vehicleBrands)
      throw new NotFoundException(`Vehicle Brand with id: ${id} not found`);
    try {
      await this.vehicleBrandRepository.save(vehicleBrands);
      return {
        message: 'Vehicle Brand updated successfully',
        data: vehicleBrands,
      };
    } catch (error) {
      this.handlerDbException(error);
    }
  }

  async remove(id: string) {
    const vehicleBrand = await this.findOne(id);
    this.vehicleBrandRepository.remove(vehicleBrand.data);
    return {
      message: 'Vehicle Brand deleted successfully',
    };
  }

  private handlerDbException(error: any) {
    this.logger.error(error);

    if (error.code === '23505') {
      throw new BadRequestException(error.detail);
    }
    throw new InternalServerErrorException();
  }
}
