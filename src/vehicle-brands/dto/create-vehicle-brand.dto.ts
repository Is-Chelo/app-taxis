import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, MinLength } from 'class-validator';

export class CreateVehicleBrandDto {
  @ApiProperty({
    description: 'The name of the vehicle brand',
    example: 'Toyota',
    required: true,
  })
  @IsNotEmpty()
  @IsString()
  @MinLength(1)
  name: string;
}
